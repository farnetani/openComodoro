from rest_framework import serializers

from core.models import *


class EquipeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Equipe


class AcademiaSerializer(serializers.ModelSerializer):
    equipe_display = serializers.SerializerMethodField()

    def get_equipe_display(self, obj):
        return obj.equipe.nome

    class Meta:
        fields = '__all__'
        model = Academia


class ProfessorSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Professor


class InscricaoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Inscricao
