from rest_framework import viewsets

from rest.serializers import *


class EquipeViewSet(viewsets.ModelViewSet):
    serializer_class = EquipeSerializer
    queryset = Equipe.objects.all()


class AcademiaViewSet(viewsets.ModelViewSet):
    serializer_class = AcademiaSerializer
    queryset = Academia.objects.all()


class ProfessorViewSet(viewsets.ModelViewSet):
    serializer_class = ProfessorSerializer
    queryset = Professor.objects.all()


class InscricaoViewSet(viewsets.ModelViewSet):
    serializer_class = InscricaoSerializer
    queryset = Inscricao.objects.all()
