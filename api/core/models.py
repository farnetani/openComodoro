from django.db import models


class Equipe(models.Model):
    nome = models.CharField(max_length=100)
    observacao = models.TextField(null=True, blank=True)
    logo = models.ImageField(upload_to='equipes', null=True)

    def __str__(self):
        return self.nome


class Academia(models.Model):
    nome = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    estado = models.CharField(max_length=2)
    equipe = models.ForeignKey('core.Equipe', on_delete=models.CASCADE)

    def __str__(self):
        return self.nome


class Professor(models.Model):
    academia = models.ForeignKey('core.Academia', on_delete=models.CASCADE)
    nome = models.CharField(max_length=100)
    email = models.EmailField(null=True, blank=True)
    telefone = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.nome


class Inscricao(models.Model):
    SEXO = (
        ('M', 'Masculino'),
        ('F', 'Feminino'),
    )
    FAIXA = (
        (1, 'Branca'),
        (2, 'Cinza / Branca'),
        (3, 'Cinza'),
        (4, 'Cinza / Preta'),
        (5, 'Amarela Branca'),
        (6, 'Amarela'),
        (7, 'Amarela / Preta'),
        (8, 'Laranja / Branca'),
        (9, 'Laranja'),
        (10, 'Laranja / Preta'),
        (11, 'Verde / Branca'),
        (12, 'Verde'),
        (13, 'Verde / Preta'),
        (14, 'Azul'),
        (15, 'Roxa'),
        (16, 'Marrom'),
        (17, 'Preta'),
        (18, 'Vermelha / Preta'),
        (19, 'Vermelha / Branca'),
        (20, 'Vermelha'),
    )
    CATEGORIA_PESO = (
        (1, 'Galo'),
        (2, 'Pluma'),
        (3, 'Pena'),
        (4, 'Leve'),
        (5, 'Médio'),
        (6, 'Meio Pesado'),
        (7, 'Pesado'),
        (8, 'Super Pesado'),
        (9, 'Pesadíssimo'),
    )
    CATEGORIA_IDADE = (
        (1, 'Pré-Mirin'),
        (2, 'Mirin'),
        (3, 'Infantil'),
        (4, 'Infanto Juvenil'),
        (7, 'Juvenil'),
        (5, 'Adulto'),
        (6, 'Master'),
    )

    data = models.DateTimeField(auto_now_add=True)
    nome = models.CharField(max_length=100)
    sexo = models.CharField(max_length=1, choices=SEXO)
    categoria_idade = models.IntegerField('Idade', choices=CATEGORIA_IDADE, default=5)
    faixa = models.IntegerField(choices=FAIXA, default=1)
    categoria_peso = models.IntegerField('Peso', choices=CATEGORIA_PESO, default=1)
    absoluto = models.BooleanField(default=False)
    valor = models.DecimalField(max_digits=15, decimal_places=2, default=50, null=True, blank=True)
    valor_absoluto = models.DecimalField(max_digits=15, decimal_places=2, default=70, null=True, blank=True)
    pago_inscricao = models.BooleanField(default=False)
    pago_absoluto = models.BooleanField(default=False)
    professor = models.ForeignKey('core.Professor', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'inscrição'
        verbose_name_plural = 'incrições'

    def __str__(self):
        return self.nome
