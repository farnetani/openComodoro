import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../plugins/axios'

Vue.use(Vuex)

const state = {
  equipes: [],
  academias: [],
  professores: []
}
const mutations = {
  REMOVE: (state, payload) => {
    state[payload.state].forEach((item, index) => {
      if (item.id === payload.data) {
        state[payload.state].splice(index, 1)
      }
    })
  },
  APPLY: (state, payload) => {
    if (Array.isArray(payload.data)) {
      state[payload.state] = payload.data
    } else {
      let update = state[payload.state].filter(
        f => f.id === payload.data.id
      ).length > 0
      if (update) {
        state[payload.state] = state[payload.state].map(
          m => m.id === payload.data.id ? payload.data : m
        )
      } else {
        state[payload.state].push(payload.data)
      }
    }
  }
}

const parseData = (context, {commit, state}, data) => {
  context.commit('APPLY', {state, data})
}

const removeData = (context, {commit, state}, data) => {
  context.commit('REMOVE', {state, data})
}

const actions = {
  load: (context, payload) => axios.get(payload.url, payload.fiters)
    .then(data => parseData(context, payload, data.data)),
  new: (context, payload) => axios.post(payload.url, payload.data)
    .then(data => parseData(context, payload, data.data)),
  update: (context, payload) => axios.patch(`${payload.url}${payload.data.id}/`, payload.data)
    .then(data => parseData(context, payload, data.data)),
  remove: (context, payload) => {
    axios.delete(`${payload.url}${payload.data.id}/`)
      .then(data => removeData(context, payload, payload.data.id))
  }
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})
