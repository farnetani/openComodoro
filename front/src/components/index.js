import Vue from 'vue'
import Countdown from './Countdown'
import EquipeForm from './EquipeForm'
import AcademiaForm from './AcademiaForm'

Vue.component('countdown', Countdown)
Vue.component('form-academia', AcademiaForm)
Vue.component('form-equipe', EquipeForm)
