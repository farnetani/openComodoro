import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Equipe from '@/views/Equipe'
import Academia from '@/views/Academia'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/equipes',
      name: 'Equipes',
      component: Equipe
    },
    {
      path: '/academias',
      name: 'Academias',
      component: Academia
    }
  ]
})
